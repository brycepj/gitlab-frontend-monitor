## Overview

Prometheus will send probes at a regular interval to our Node.js server, which in turn will prompt Selenium
to open a new session in a Phantom.js headless browser, visit a specific target(url), execute a metric-gathering script, and
return the results formatted in a way Prometheus can digest.

## Setup

To run gitlab-frontend-monitor, you need to be running an instance of prometheus, a selenium server, and scrape server
simultaneously. You will also need to have Phantom.js installed on your machine.

### Install and run Prometheus

To run a local instance of Orometheus, follow setup instructions [here](https://prometheus.io/docs/introduction/install/).

Then navigate to the binary's source directory and execute the following:

```
./prometheus --config.file='path/to/this_repo/gitlab-frontend-monitor/prometheus.yml'
```

## Start the Node.js server

```
yarn # install server dependencies
node server/server.js # start listening on port 9156
```

TODO:

- update docs for headless chrome (https://developers.google.com/web/updates/2017/04/headless-chrome)
- add ci script for terraform to call
- implement performance timings collections in headless chrome





















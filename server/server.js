
const fs = require('fs');
const path = require('path');

const _ = require('lodash');
const promClient = require('prom-client');

const express = require('express');
const app = express();

const bodyParser = require('body-parser');

const probeHandler = require('../lib/probes/performance_timings/exporter');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// create a write stream (in append mode) for morgan to log to

app.use(express.static('public'));
app.set('view engine', 'pug')

app.get('/probe', (req, res) => {
  probeHandler()
    .then((formattedResults) => {
      res.send(promClient.register.metrics());
    })
    .catch((err) => {
      console.error(err);
    });
});

app.get('/metrics', (req, res) => {
  res.send(promClient.register.getMetricsAsJSON());
});

// 9156 is the default port for prometheus WebExporter
app.listen(9156, () => {
  console.log('Frontend monitor listening on port 9156!');
});
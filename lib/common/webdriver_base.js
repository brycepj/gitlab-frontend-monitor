/** 
 * For catching configuration and runtime errors, and ending
 * webdriver sessions _after_ results have been returned.
 */

const webdriverConfigLookup = {
  performance_timings: require('../probes/performance_timings/webdriver'),
};

module.exports = (jobKey, jobConfig) => {
  if (!jobKey) {
    throw Error('You need to specify a job to run.');
  }

  if (!webdriverConfigLookup.hasOwnProperty(jobKey)) {
    throw Error(`${jobKey} is an invalid job key.`);
  }

  if (!jobConfig) {
    throw Error('Provide neccessary configuration to run job.');
  }

  return new Promise((resolve, reject) => {
    webdriverConfigLookup[jobKey](jobConfig)
      .then((results) => {
        console.log(`${jobKey} ran successfully for ${jobConfig} -- ${new Date()}`);
        resolve(results.value);
      })
      .catch((err) => {
        console.error(err);
        reject(err);
      })
      .end();
  });
}
const lighthouse = require('lighthouse');
const chromeLauncher = require('lighthouse/chrome-launcher');
const perfConfig = require('lighthouse/lighthouse-core/config/perf.json');

function launchChromeAndRunLighthouse(url, flags = {}, config = null) {
  return chromeLauncher.launch().then(chrome => {
    flags.port = chrome.port;
    return lighthouse(url, flags, perfConfig).then(results =>
      chrome.kill().then(() => results));
  });
}

const flags = {};

const url = 'https://gitlab.com/gitlab-org/gitlab-ce';

module.exports = () => {
  return launchChromeAndRunLighthouse(url, flags, perfConfig);
};



const { Launcher } = require('lighthouse/chrome-launcher/chrome-launcher');
/**
 * Launches a debugging instance of Chrome on port 9222, returns a promise
 */

const headless = true;

function launchChrome(headless = true) {
  const launcher = new Launcher({
    port: 9222,
    autoSelectChrome: true,
    additionalFlags: [
      '--window-size=1280,1696',
      '--disable-gpu',
      headless ? '--headless' : ''
    ]
  });

  return launcher.launch().then(() => launcher)
    .catch(err => {
      // Kill Chrome if there's an error.
      return launcher.kill().then(() => {
        throw err;
      }, console.error);
    });
}

module.exports = launchChrome;

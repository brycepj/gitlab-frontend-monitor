const chrome = require('chrome-remote-interface');
const launchChrome = require('../common/launch_chrome');

function onPageLoad(Profiler) {
  // return Page.getAppManifest().then(response => {
    // if (!response.url) {
      // console.log('Site has no app manifest');
      // return;
    // }
    // console.log('Manifest: ' + response.url);
    // console.log(response.data);
//  });

  Profiler.enable();
  Profiler.start();
  return Promise.resolve();
}

launchChrome().then(launcher => {
  chrome(protocol => {
    // See protocol API docs: https://chromedevtools.github.io/devtools-protocol/
    const {Page, Runtime, Profiler } = protocol;

     Page.enable().then(() => {
      Page.navigate({url: 'https://www.chromestatus.com/'});

      Page.loadEventFired(() => {
        onPageLoad(Profiler).then(() => {
          setTimeout(() => {
            Profiler.stop().then((results) => {
              console.log(results);
              protocol.close();
              launcher.kill(); // Kill Chrome. Probably want to remove this.
            });
          }, 5000);
        });
      });
    });

  }).on('error', err => {
    throw Error('Cannot connect to Chrome:' + err);
  });
});

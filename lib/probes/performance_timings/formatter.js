const _ = require('lodash');
const promClientMetrics = require('./metrics');

module.exports = (timings, url) => {
  const computed_timings = {
    frontend: timings.loadEventEnd - timings.domLoading,
    html_parsing: timings.domInteractive - timings.domLoading,
    dom_construction: timings.domContentLoadedEventStart - timings.domLoading,
    dom_interactive: timings.domInteractive - timings.navigationStart,
  };

  _.each(computed_timings, (val, key) => {
    promClientMetrics[key].observe({
      target: url,
    }, (val / 1000));
  });

  return computed_timings;
};
module.exports = {
  static_config_targets: [
    "https://gitlab.com/gitlab-org/gitlab-ee/merge_requests",
    "https://gitlab.com/gitlab-org/gitlab-ee/merge_requests/1126",
    "https://gitlab.com/gitlab-org/gitlab-ee/issues/928",
    "https://gitlab.com/gitlab-org/gitlab-ee/pipelines",
    "https://gitlab.com/brycepj"
  ]
}
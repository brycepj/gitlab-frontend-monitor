const webdriverio = require('webdriverio');

module.exports = (url) => {
  const webdriver_config = {
    desiredCapabilities: {
      browserName: 'phantomjs' 
    }
  };

  return webdriverio
    .remote(webdriver_config)
    .init()
    .url(url)
    .execute('return window.performance.timing');
};

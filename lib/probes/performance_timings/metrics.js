const promClient = require('prom-client');
const defaultBuckets = [.01, .05, .1, .25, .5, 1, 5, 10];
const defaultLabels = ['target'];

module.exports = {
  frontend: new promClient.Histogram(generate_metric_name('frontend'), 'time between server response and DOM ready', defaultLabels, {
    buckets: defaultBuckets
  }),
  html_parsing: new promClient.Histogram(generate_metric_name('html_parsing'), 'time the browser spends parsing html on pageload', defaultLabels, {
    buckets: defaultBuckets
  }),
  dom_construction: new promClient.Histogram(generate_metric_name('dom_construction'), 'time the browser spent constructing the DOM', defaultLabels, {
    buckets: defaultBuckets
  }),
  dom_interactive: new promClient.Histogram(generate_metric_name('dom_interactive'), 'time it took for the DOM to be interactive', defaultLabels, {
    buckets: defaultBuckets
  }),
}

function generate_metric_name(metric_key) {
  const namespace = 'frontend';
  const job_name = 'performance_timings';
  const unit = 'seconds';

  return [namespace, job_name, metric_key, unit].join('_');
}
const _ = require('lodash');

const targets = require('./config').static_config_targets;

const baseExporter = require('../../common/webdriver_base');
const formatter = require('./formatter');

module.exports = () => {
  // this is a cheap way to iterate through the targets list in memory
  const nextUrl = targets[targets.push(targets.shift()) - 1];

  return baseExporter('performance_timings', nextUrl)
    .then((results) => {
      return formatter(results, nextUrl);
    })
    .catch((err) => {
      console.error(err);
    });
};
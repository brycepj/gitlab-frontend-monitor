const runLighthouse = require('../common/run_lighthouse');

runLighthouse().then((results) => {
  const audits = results.audits;
  for (metric in audits) {
    console.log(`${metric}: ${audits[metric].displayValue}`);
  }
});
